# Windows Microservice With Jar Executable
![alt text](https://img.shields.io/badge/-Java-brightgreen) ![alt text](https://img.shields.io/badge/-Maven-brightgreen) ![alt text](https://img.shields.io/badge/-Spring%20boot%20CLI-brightgreen) ![alt text](https://img.shields.io/badge/-Jar-brightgreen) ![alt text](https://img.shields.io/badge/-Winsw-brightgreen)

This is example to create windows service with jar executable. This jar is simple java program using "Spring-boot" to say greeting.
Example: "Hello World!" (default) or "Hello << something >>!" (if post something value on url)

## Requirement
1. Install **Java**
    - [Go to download page Java installer here!](https://www.java.com/en/download/)
    - Download jdk windows installer (**Java.exe**)
    - Runing installer by double click on **Java.exe**
2. Set Envirounment varibale **JAVA_HOME** (Latter on to compile java source code and packaging jar)
    - Open windows explorer
    - Right click on **This PC**
    - Choose **properties**
    - Click **Advenced system settings** on left side menu
    - On Tab `Advanced` click button **Environment Variable** on bottom pop-up
    - On the section `Envirounment properties` (second section) click button **New**
    - Fill form dialog `Variable name` with **JAVA_HOME** and `Variable value` with **location of JDK installed** then click **OK**
    > Example: Variable name=**JAVA_HOME** & Varibale value=**C:\Program Files\Java\jdk1.8.0_241**
    - We should add **JAVA_HOME** on `Path` too. So select `Path` on the section `System variables` then click **Edit**
    - Click button **New** on pop-up `Edit environment variable`
    - Fill value **JAVA_HOME** with extra **\bin** then click **OK**
    > Example **%JAVA_HOME%\bin**
    - Click **OK** on pop-up `Envirounment variables`
    - Click **OK** on pop-up `System Properties`
    - To confirm all setting **JAVA_HOME** is correct, open **CMD** (click button `windows` find `Command Prompt`) then run command below:
    > java -version && javac -version
3. Install **Maven**
    - [Go to download page Maven installer here!](https://maven.apache.org/download.cgi)
    - Download **apache-maven-3.6.3-bin.zip**
    - Extract downloaded file then copy directory (result from extraction on location you want)
    > Example: C:\apache-maven-3.6.3
    - Set environment variable like step-2 by named whatever you want
    > Example: **MAVEN_HOME=C:\apache-maven-3.6.3** && Path=**%MAVEN_HOME%\bin**
    - To confirm all setting **Maven** is correct, open **CMD** (click button `windows` find `Command Prompt`) then run command below:
    > mvn --version
4. Install **Spring-boot CLI**
    - [Click here detail documentation!](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#getting-started-installing-spring-boot)
    - Recommanded choose **Manual Installation**
    - By link above (on detail documentation) section `Manual Installation` download **spring-boot-cli-2.2.4.RELEASE-bin.zip**
    - Extract downloaded file then copy directory (result from extraction on location you want)
    > Example: C:\spring-2.4.4.RELEASE
    - Set environment variable like step-2 by named whatever you want
    > Example: **SPRINGBOOT_HOME=C:\spring-2.4.4.RELEASE** && Path=**%SPRINGBOOT_HOME%\bin**
    - FYI on directory `bin` have 2 type, **spring** file for **Linux & Mac** and **spring.bat** for **Windows**
    - To confirm all setting **Spring-boot CLI** is correct, open **CMD** (click button `windows` find `Command Prompt`) then run command below:
    > spring --version
5. Prepare **Windows Service Wrapper (Winsw)** (Latter on to make windows service)
    - [Go to download page Winsw installer here!](https://repo.jenkins-ci.org/releases/com/sun/winsw/winsw/)
    - Choose latest version available (existing is **2.4.0**)
    - Download **winsw-2.4.0-bin.exe**

# Create Java Application Using Spring-boot CLI
- [For detail is here!](/spring-boot)

# Create Jar File As Windows Service
- [For detail is here!](/windows-service)